<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Repository\ProductRepository;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\ShoppingCart;
use App\Entity\ProductLine;



class ProductLineController extends Controller
{
    /**
     * @Route("/product/line", name="product_line")
     */
    public function createCart(UserInterface $user, int $number=1, int $id, ProductRepository $repo, ObjectManager $manager)
    {

        if(!$session) {
            $session = new Session();
            }
            $session->start();
    
            $cart = $session->get("cart");
    
            if(!$cart) {
                $cart = new ShoppingCart();
            } else {
                $cart = $manager->merge($cart);
               
            }
    
            $productLine = new ProductLine();
    
            $repo = $repo->find($id);
    
            $productLine->setProduct($repo);
    
            $productLine->setQuantity($number);
            
            $price = $repo->getPrice() * $number;
    
            $productLine->setPrice($price);
    
            $cart->addProductLine($productLine);
    
            $cart->setUser($user);
            
            $session->set("cart", $cart);
            
            $manager->persist($cart);
            $manager->flush();
    
            return $this->redirectToRoute("home", []);
        }
    
        /**
        *  @Route("/user/{id}/remove-productLine", name="remove_productLine")
        */
        public function remove(ProductLine $productLine) {
    
            $em = $this->getDoctrine()->getEntityManager();
            $em->remove($productLine);
            $em->flush();
    
            return $this->redirectToRoute("shopping_cart", []);
        }
    
    }
    