<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Product;

class ShoppingCartController extends Controller
{
    /**
     * @Route("/shopping/cart", name="shopping_cart")
     */
    public function index(Product $product)
    {
        $product = new Product();

        $em = $this->getDoctrine()->getManager();

        $em->persist($product);

        $em->flush();

        return $this->render('shopping_cart/index.html.twig', [
        ]);
    }
}
