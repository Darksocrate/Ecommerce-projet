<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Category;
use App\Form\CategoryType;

class AddCategoryController extends Controller{

    /**
     * @Route("/add/category", name="add_category")
     */
    public function index(Request $request){

        $category = new Category();
   
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            /**
             * Pour mettre sur la bdd les informations d'un formulaire
             * on fait le formulaire comme d'hab, mais ici, on utilise
             * le manager et le persist/flush
             */
            $category = $form->getData();
          
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($category);

            $em->flush();

            {
                $article = $repo->getById($id);
        
                $form = $this->createForm(ArticleType::class, $article);
        
                $form->handleRequest($request);
        
                if ($form->isSubmitted() && $form->isValid()) {
        
                    $repo->update($form->getData());
        
                    return $this->redirectToRoute("home");
                }
                return $this->render('add-article.html.twig', [
                    'form' => $form->createView()
                ]);
            }


        } 
        return $this->render('add_category/index.html.twig', [
            'controller_name' => 'AddCategoryController',
            'form' => $form->createView()
        ]);
    }  

    /**
     * @Route("/update/category/{id}", name="update_category")
     */

    public function update(Category $category, int $id, Request $request)
    {
        $form = $this->createForm( CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('home', ["id" => $category->getId()]);
        }

        return $this->render('add_category/update-category.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));

        return $this->redirectToRoute('home', ["id" => $category->getId()]);
    }
}
